# split_methyl_bed

A quick python script with CLI that splits a bed file from megalodon into three contexts.

```
$ python split_methyl_bed.py -h
usage: split_methyl_bed.py [-h] methylbed fasta outprefix

parses a methylbed into contexts by reference to a fasta file

positional arguments:
  methylbed   a methylbed file from megalodon
  fasta       the reference genome that the methylbed is paired with
  outprefix   the prefix used for output files

optional arguments:
  -h, --help  show this help message and exit

```


# quick start

The following script outlines the steps needed to run this script assuming you have conda installed.

```
# download this repo
git clone https://gitlab.com/NolanHartwick/split_methyl_bed.git
cd split_methyl_bed
# create an env satisfying requirements if needed
conda env create -n split_bed -f requirements.yml
# activate your env
conda activate split_bed
# run the script
python split_methyl_bed.py some_bed_file.bed matching.fasta some_prefix
```

